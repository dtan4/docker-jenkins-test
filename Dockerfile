FROM jenkins:1.642.2
MAINTAINER Daisuke Fujita <dtanshi45@gmail.com> (@dtan4)

ENV DOCKER_VERSION 1.10.2

USER root

RUN apt-get update && \
    apt-get install -y sudo && \
    rm -rf /var/lib/apt/lists/*
RUN wget -qO /usr/local/bin/docker https://get.docker.com/builds/Linux/x86_64/docker-$DOCKER_VERSION && \
    chmod +x /usr/local/bin/docker
RUN echo "jenkins ALL=NOPASSWD: ALL" >> /etc/sudoers

USER jenkins

COPY plugins.txt /usr/share/jenkins/plugins.txt
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/plugins.txt
